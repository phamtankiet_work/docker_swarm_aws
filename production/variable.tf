variable "aws_region" {
  description = "AWS region on which we will setup the swarm cluster"
  default     = "ap-southeast-1"
}

variable "swarm_worker_ami" {
  description = "Amazon Linux AMI"
  default     = "ami-062550af7b9fa7d05" // ubuntu server 20.04
}

variable "swarm_manager_ami" {
  description = "Amazon Linux AMI"
  default     = "ami-062550af7b9fa7d05" // ubuntu server 20.04

}

variable "swarm_manager_instance_type" {
  description = "Instance type"
  default     = "t3.micro"

}

variable "swarm_worker_instance_type" {
  description = "Instance type"
  default     = "t3.micro"
}

variable "swarm_manager_user" {
  description = "SSH User"
  default     = "ubuntu"
}

variable "swarm_worker_user" {
  description = "SSH User"
  default     = "ubuntu"

}

variable "swarm_manager_count" {
  description = "Number of managers"
  default     = 1

}

variable "swarm_worker_count" {
  description = "Number of workers"
  default     = 3
}

variable "ssh_key_path" {
  description = "SSH Public Key path"
  default     = "swarm_key.pem"
}

variable "ssh_key_name" {
  description = "Desired name of Keypair..."
  default     = "swarm_key"
}

variable "bootstrap_path" {
  description = "Script to install Docker Engine"
  default     = "install_docker_machine_compose.sh"
}

variable "additional_tags" {
  default     = { "terraform" = "true", "terraform_id" = "11" }
  description = "Additional resource tags"
  type        = map(string)
}

variable "availability_zones" {
  default     = ["ap-southeast-1a", "ap-southeast-1b", "ap-southeast-1c"]
  description = "List of availability zones"
  type        = list(string)
}

variable "swarm_manager_spot_price" {
  default     = "0.1"
  description = "The maximum hourly price (bid) you are willing to pay for the instance, e.g. 0.10"
}

variable "instance_interruption_behavior" {
  default     = "stop"
  description = "whether a Spot instance stops or terminates when it is interrupted, can be stop or terminate"
}

variable "swarm_worker_spot_price" {
  default     = "0.1"
  description = "The maximum hourly price (bid) you are willing to pay for the instance, e.g. 0.10"
}
