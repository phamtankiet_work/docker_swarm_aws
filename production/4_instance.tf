resource "aws_spot_instance_request" "swarm_manager" {
  count = var.swarm_manager_count
  tags = merge(
    var.additional_tags,
    {
      Name = "docker_swarm_manager_${count.index + 1}"
    },
  )

  spot_price                     = var.swarm_manager_spot_price
  wait_for_fulfillment           = "true"
  spot_type                      = "persistent"
  instance_interruption_behavior = var.instance_interruption_behavior

  ami           = var.swarm_manager_ami
  instance_type = var.swarm_manager_instance_type
  key_name      = var.ssh_key_name

  subnet_id = aws_subnet.swarm_subnet.id
  vpc_security_group_ids = [
    aws_security_group.docker_swarm_node_sg.id,
    aws_security_group.http_https_access.id,
    aws_security_group.ssh_access.id
  ]
  associate_public_ip_address = true


  root_block_device {
    volume_size = 8
    volume_type = "gp2"
  }
}
resource "aws_spot_instance_request" "swarm_worker" {
  count = var.swarm_worker_count
  tags = merge(
    var.additional_tags,
    {
      Name = "docker_swarm_worker_${count.index + 1}"
    },
  )

  spot_price                     = var.swarm_worker_spot_price
  wait_for_fulfillment           = "true"
  spot_type                      = "persistent"
  instance_interruption_behavior = var.instance_interruption_behavior


  ami           = var.swarm_worker_ami
  instance_type = var.swarm_worker_instance_type
  key_name      = var.ssh_key_name

  subnet_id = aws_subnet.swarm_subnet.id
  vpc_security_group_ids = [
    aws_security_group.docker_swarm_node_sg.id,
    aws_security_group.ssh_access.id
  ]
  associate_public_ip_address = true


  root_block_device {
    volume_size = 8
    volume_type = "gp2"
  }
}

resource "local_file" "ansible_inventory" {
  content = join("\n", [
    "[swarm_manager]",
    join("\n", [
      for instance in aws_spot_instance_request.swarm_manager : "${instance.public_ip} ansible_user=${var.swarm_manager_user} ansible_ssh_private_key_file=${var.ssh_key_path}"
    ]),
    "[swarm_worker]",
    join("\n", [
      for instance in aws_spot_instance_request.swarm_worker : "${instance.public_ip} ansible_user=${var.swarm_manager_user} ansible_ssh_private_key_file=${var.ssh_key_path}"
    ])
  ])

  filename = "${path.module}/ansible/inventory.ini"
}

output "master_public_ip" {
  value = ["${aws_spot_instance_request.swarm_manager.*.public_ip}"]
}
output "worker_public_ip" {
  value = ["${aws_spot_instance_request.swarm_worker.*.public_ip}"]
}
