# Create a VPC and subnets for the Swarm nodes
resource "aws_vpc" "swarm_vpc" {
  cidr_block = "10.0.0.0/20"

  # enable_dns_hostnames = true
  # enable_dns_support   = true

  tags = merge(
    var.additional_tags,
    {
      Name = "swarm_vpc"
    },
  )
}
resource "aws_subnet" "swarm_subnet" {
  vpc_id     = aws_vpc.swarm_vpc.id
  cidr_block = "10.0.0.0/24"

  availability_zone = var.availability_zones[0]

  tags = merge(
    var.additional_tags,
    {
      Name = "swarm_private_subnet"
    },
  )
}

resource "aws_internet_gateway" "swarm_internet_gateway" {
  vpc_id = aws_vpc.swarm_vpc.id

  tags = {
    Name = "swarm_internet_gateway"
  }
}

resource "aws_route_table" "swarm_route_table" {
  vpc_id = aws_vpc.swarm_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.swarm_internet_gateway.id
  }

  tags = {
    Name = "swarm_route_table"
  }
}

resource "aws_route_table_association" "swarm_route_table_swarm_internet_gateway" {
  subnet_id      = aws_subnet.swarm_subnet.id
  route_table_id = aws_route_table.swarm_route_table.id
}
