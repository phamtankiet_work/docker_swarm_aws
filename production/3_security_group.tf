# Create a security group for the Swarm cluster
resource "aws_security_group" "docker_swarm_node_sg" {
  vpc_id      = aws_vpc.swarm_vpc.id
  description = "Security group for Docker Swarm nodes"

  ingress {
    description = "Allow incoming traffic on port 2377 from Swarm workers, for communication with and between manager nodes"
    from_port   = 2377
    to_port     = 2377
    protocol    = "tcp"
    self        = true
  }

  ingress {
    description = "Allow incoming traffic on port 7946 from other Swarm nodes, for overlay network node discovery"
    from_port   = 7946
    to_port     = 7946
    protocol    = "tcp"
    self        = true
  }

  ingress {
    description = "Allow incoming traffic on port 4789 from other Swarm nodes, for overlay network traffic"
    from_port   = 4789
    to_port     = 4789
    protocol    = "udp"
    self        = true
  }

  # Enable ICMP
  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "Allow all outgoing traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  name = "docker_swarm_security_group"
  tags = merge(
    var.additional_tags,
    {
      Name = "docker_swarm_security_group"
    },
  )
}

resource "aws_security_group" "http_https_access" {
  vpc_id = aws_vpc.swarm_vpc.id

  description = "Allow HTTP and HTTPS access"
  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  name = "http_https_public_access"
  tags = merge(
    var.additional_tags,
    {
      Name = "http_https_public_access"
    },
  )
}

resource "aws_security_group" "ssh_access" {
  vpc_id      = aws_vpc.swarm_vpc.id
  description = "Allow SSH access"
  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  name = "ssh_access"
  tags = merge(
    var.additional_tags,
    {
      Name = "ssh_access"
    },
  )
}
